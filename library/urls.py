from django.urls import path, include
from library import views
from rest_framework import routers

# authors_list = views.AuthorViewSet.as_view(actions={"get": "list", "post": "create"})
# authors_detail = views.AuthorViewSet.as_view(
#     {
#         "get": "retrieve",
#         "put": "update",
#         "patch": "partial_update",
#         "delete": "destroy",
#     }
# )
#
# urlpatterns = [
#     path(
#         "authors/",
#         authors_list,
#         name="author-list",
#     ),
#     path(
#         "authors/<int:pk>/",
#         authors_detail,
#         name="author-detail",
#     ),
# ]

router = routers.DefaultRouter()
router.register("authors", views.AuthorViewSet)
router.register("genres", views.GenreViewSet)
router.register("books", views.BookViewSet)

urlpatterns = [path("", include(router.urls))]

app_name = "library"
