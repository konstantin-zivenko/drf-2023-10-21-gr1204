from rest_framework import serializers

from library.models import Author, Genre, Book


class AuthorSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    year_of_birth = serializers.DateField(required=False, allow_null=True)

    def create(self, validated_data):
        """
        Create and return a new `Author` instance, given the validated data.
        """
        return Author.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Author` instance, given the validated data.
        """
        instance.first_name = validated_data.get("first_name", instance.first_name)
        instance.year_of_birth = validated_data.get(
            "year_of_birth", instance.year_of_birth
        )
        instance.last_name = validated_data.get("last_name", instance.last_name)
        instance.save()
        return instance


# class GenreSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     name = serializers.CharField(required=True)
#
#     def create(self, validated_data):
#         """
#         Create and return a new `Genre` instance, given the validated data.
#         """
#         return Genre.objects.create(**validated_data)
#
#     def update(self, instance, validated_data):
#         """
#         Update and return an existing `Author` instance, given the validated data.
#         """
#         instance.name = validated_data.get("name", instance.name)
#         instance.save()
#         return instance


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ("id", "name")


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ("id", "title", "description", "published", "author", "genre")


class BookListSerializer(BookSerializer):
    author = AuthorSerializer(many=True, read_only=True)
