from django.db import models


class Author(models.Model):
    first_name = models.CharField(max_length=31)
    last_name = models.CharField(max_length=31)
    year_of_birth = models.DateField(null=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    class Meta:
        ordering = ["last_name"]


class Genre(models.Model):
    name = models.CharField(max_length=15)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class Book(models.Model):
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=4095, null=True, blank=True)
    author = models.ManyToManyField(Author)
    genre = models.ManyToManyField(Genre)
    published = models.IntegerField(null=True)

    def __str__(self):
        return f"{self.title}, published: {self.published}"

    class Meta:
        ordering = ["title"]
